const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
camera.position.z = 5;

let mouse = new THREE.Vector2();
let touchEvents = [];

const light = new THREE.PointLight( 0xFFFFFF, 1, 100 );
light.position.set( 0, 0, 5 );
scene.add( light );

const renderer = new THREE.WebGLRenderer();

renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

let bodyHeight = $( "body" ).height();
let scrollPercentage;
$( window ).on( 'scroll', function(){
  scrollPercentage = ( $( window ).scrollTop() ) / (bodyHeight - window.innerHeight) * 100;
  renderer.setSize( window.innerWidth, window.innerHeight );
  camera.aspect = window.innerWidth / window.innerHeight;
  scrollEvent();
});

let drawingContainer = new THREE.Object3D();
scene.add( drawingContainer );

let isMouseDown = false;

let strokeCount = 0;

$( "body" ).on( "mousemove", function( evento ){

  if( isMouseDown ){

    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	  mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

    let vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
    vector.unproject( camera );
    let dir = vector.sub( camera.position ).normalize();
    let distance = ((camera.position.z - 2) - camera.position.z) / dir.z;
    let pos = camera.position.clone().add( dir.multiplyScalar( distance ) );

    let radius = (0.1) - (strokeCount * 0.001);
    if( radius <= 0 ){
      radius = 0;
    }

    let geometry = new THREE.CircleGeometry( radius, 32 );
    let material = new THREE.MeshBasicMaterial( { color: 0xffff00 } );
    let circle = new THREE.Mesh( geometry, material );
    circle.position.x = pos.x;
    circle.position.y = pos.y;
    circle.position.z = (pos.z) + (strokeCount * 0.01);
    drawingContainer.add( circle );

    let radius2 = (0.102) - (strokeCount * 0.001);

    if( radius2 <= 0 ){
      radius2 = 0;
    }

    let geometry2 = new THREE.CircleGeometry( radius2, 32 );
    let material2 = new THREE.MeshBasicMaterial( { color: 0x000000 } );
    let circle2 = new THREE.Mesh( geometry2, material2 );
    circle2.position.x = pos.x;
    circle2.position.y = pos.y;
    circle2.position.z = (pos.z - 0.001) + (strokeCount * 0.01);
    drawingContainer.add( circle2 );

    /*let dGeo = new THREE.BoxGeometry( 0.1, 0.1, 0.1 );
    let dMat = new THREE.MeshLambertMaterial({ color: 0xFFFFFF });
    let dElement = new THREE.Mesh( dGeo, dMat );
    dElement.position.x = pos.x;
    dElement.position.y = pos.y;
    dElement.position.z = pos.z;
    drawingContainer.add( dElement );*/
    console.log( pos.z );

    strokeCount++;
  }//isMouseDown
});

$( "body" ).on( "mousedown", function( evento ){
  isMouseDown = true;
});

$( "body" ).on( "mouseup", function( evento ){
  isMouseDown = false;
  strokeCount = 0;
});

$( "body" ).on( "touchmove", function( evento ){
  console.log( evento );
  $( "#console" ).html( "" );
  touchEvents = [];
  for( let i = 0; i < evento.changedTouches.length; i++ ){
    let coordX = ( evento.changedTouches[ i ].clientX / window.innerWidth ) * 2 - 1;
    let coordY = - ( evento.changedTouches[ i ].clientY / window.innerHeight ) * 2 + 1;

    let vector = new THREE.Vector3(coordX, coordY, 0.5);
    vector.unproject( camera );
    let dir = vector.sub( camera.position ).normalize();
    let distance = ((camera.position.z - 2) - camera.position.z) / dir.z;
    let pos = camera.position.clone().add( dir.multiplyScalar( distance ) );

    touchEvents.push( new THREE.Vector3( pos.x, pos.y, pos.z ) );
    $( "#console" ).append( "x: " + touchEvents[ i ].x + ", y: " + touchEvents[ i ].y + "<br>" );
    let dGeo = new THREE.BoxGeometry( 0.1, 0.1, 0.1 );
    let dMat = new THREE.MeshLambertMaterial({ color: 0xFFFFFF });
    let dElement = new THREE.Mesh( dGeo, dMat );
    dElement.position.x = touchEvents[ i ].x;
    dElement.position.y = touchEvents[ i ].y;
    dElement.position.z = touchEvents[ i ].z;
    drawingContainer.add( dElement );
  }

});

//let mouseIcoGeo = new THREE.BoxGeometry( 0.5, 0.5, 0.5 );
//let mouseIcoMat = new THREE.MeshLambertMaterial( { color: 0xFF6699 } );
//let mouseIco = new THREE.Mesh( mouseIcoGeo, mouseIcoMat );
//mouseIco.position.z = -10;
//scene.add( mouseIco );

let icoContainer = new THREE.Object3D();
scene.add( icoContainer );

let count = 300;
let minRad = 0.01;
let maxRad = 0.5;
let maxZ = 100;

for( let i = 0; i < count; i++ ){
  let radius = minRad + ( Math.random() * maxRad );
  let geometry = new THREE.IcosahedronGeometry( radius, 0 );
  let material = new THREE.MeshLambertMaterial( { color: 0xFFFFFF } );
  let ico = new THREE.Mesh( geometry, material );
  ico.position.x = -9 + (Math.random() * 18);
  ico.position.y = -9 + (Math.random() * 18);
  ico.position.z = - (Math.random() * 150);

  icoContainer.add( ico );
}

let geometry = new THREE.BoxGeometry( 0.7, 0.7, 0.7 );
let material = new THREE.MeshLambertMaterial({ color: 0x0000FF });
let movingBox = new THREE.Mesh( geometry, material );
movingBox.position.z = -20;
scene.add( movingBox );

let geometry2 = new THREE.BoxGeometry( 1, 1, 1 );
let material2 = new THREE.MeshLambertMaterial({ color: 0x00FF00 });
let crossingBox = new THREE.Mesh( geometry2, material2 );
crossingBox.position.z = -50;
crossingBox.position.x = -10;
scene.add( crossingBox );

let runAnimation1 = false;

function animate(){
  requestAnimationFrame( animate );

  camera.position.z = -scrollPercentage;
  light.position.z = -scrollPercentage;

	renderer.render( scene, camera );

  if( runAnimation1 == true ){
    if( crossingBox.position.x <= 10 ){
      crossingBox.position.x += 0.1;
    }
  }

  //mouseIco.position.x = mouse.x;
  //mouseIco.position.y = mouse.y;
  //mouseIco.position.z = camera.position.z - 2;


}
animate();

let firstTrigger = false;
let secondTrigger = false;

function scrollEvent(){
  //animar caja azul
  movingBox.rotation.y = toRad( scrollPercentage ) * 50;
  //console.log( scrollPercentage );
  //aparecer caja roja
  if( scrollPercentage > 10 && firstTrigger == false ){
    //console.log( "44444" );
    let geometry = new THREE.BoxGeometry( 1, 1, 1 );
    let material = new THREE.MeshLambertMaterial({ color: 0xFF0000});
    let box = new THREE.Mesh( geometry, material );
    scene.add( box );
    box.position.z = camera.position.z - 3;
    firstTrigger = true;
  }

  if( scrollPercentage > 36.5 && secondTrigger == false ){
    runAnimation1 = true;
    secondTrigger = true;
  }

  if( scrollPercentage > 50 ){
    crossingBox.position.x = -10;
  }


}

function toRad( deg ){
  return deg * ( Math.PI/180 );
}
